package com.olekdia.mdflowercolor

import android.view.View
import android.widget.EditText
import com.olekdia.common.extensions.ifNotNullAnd
import com.olekdia.flowercolorpicker.ColorPickerView
import com.olekdia.flowercolorpicker.ColorPickerView.Companion.getRenderer
import com.olekdia.flowercolorpicker.OnColorChangedListener
import com.olekdia.flowercolorpicker.slider.AlphaSlider
import com.olekdia.flowercolorpicker.slider.LightnessSlider
import com.olekdia.materialdialog.MaterialDialog

class ColorMaterialDialog(val colorBuilder: ColorMaterialDialogBuilder) : MaterialDialog(colorBuilder),
    OnColorChangedListener {

    var colorPicker: ColorPickerView

    init {
        colorPicker = view.findViewById(R.id.fcp_color_picker)
        colorPicker.onColorChangedListener = this

        colorPicker.setRenderer(getRenderer(colorBuilder.wheelType))
        colorPicker.setDensity(colorBuilder.wheelDensity)

        val lightnessSlider: LightnessSlider = view.findViewById(R.id.fcp_lightness_slider)
        if (colorBuilder.lightSliderEnabled) {
            colorPicker.setLightnessSlider(lightnessSlider)
            lightnessSlider.requestFocus()
        } else {
            lightnessSlider.visibility = View.GONE
        }

        val alphaSlider: AlphaSlider = view.findViewById(R.id.fcp_alpha_slider)
        if (colorBuilder.alphaSliderEnabled) {
            colorPicker.setAlphaSlider(alphaSlider)
        } else {
            alphaSlider.visibility = View.GONE
        }

        val colorEdit: EditText = view.findViewById(R.id.fcp_picker_edit)
        if (colorBuilder.colorEditEnabled) {
            colorEdit.setTextColor(builder.contentColor)
            colorPicker.setColorEdit(colorEdit)
        } else {
            colorEdit.visibility = View.GONE
        }

        colorPicker.initialColor = colorBuilder.initialColor
    }

    val currentColor: Int
        get() = colorPicker.currentColor

    val selectedColorInPicker: Int
        get() = colorPicker.selectedColorInPicker

    override fun onPositiveClick() {
        ifNotNullAnd(
            colorBuilder.colorCallback, !colorBuilder.alwaysCallColorCallback
        ) { callback ->
            callback.onSelection(this, currentColor)
        }
        super.onPositiveClick()
    }

    override fun onColorChanged(newColor: Int) {
        if (colorBuilder.alwaysCallColorCallback) {
            colorBuilder.colorCallback?.onSelection(this, newColor)
        }
    }

    interface ColorCallback {
        fun onSelection(
            dialog: MaterialDialog?,
            selectedColor: Int
        )
    }

    companion object {
        const val SELECTED_COLOR_KEY = "SELECTED_COLOR"
    }
}