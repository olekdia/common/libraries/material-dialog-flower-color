package com.olekdia.mdflowercolor

import android.content.Context
import android.graphics.Color
import android.view.View
import androidx.annotation.UiThread
import com.olekdia.androidcommon.extensions.getDimensionPixelSize
import com.olekdia.flowercolorpicker.ColorPickerView.WheelType
import com.olekdia.materialdialog.MaterialDialogBuilder

class ColorMaterialDialogBuilder(context: Context)
    : MaterialDialogBuilder(context) {

    @JvmField
    internal var lightSliderEnabled = true
    @JvmField
    internal var alphaSliderEnabled = false
    @JvmField
    internal var colorEditEnabled = false
    @JvmField
    @WheelType
    internal var wheelType = WheelType.FLOWER
    @JvmField
    internal var wheelDensity = 10
    @JvmField
    internal var initialColor = Color.TRANSPARENT
    @JvmField
    internal var colorCallback: ColorMaterialDialog.ColorCallback? = null
    @JvmField
    internal var alwaysCallColorCallback: Boolean = false

    init {
        super.customView(
            View.inflate(context, R.layout.fcp_full_color_picker, null),
            false
        )
        customView?.let {
            val horizPadding: Int = context.getDimensionPixelSize(R.dimen.md_dialog_frame_margin)
            it.setPadding(
                horizPadding,
                context.getDimensionPixelSize(R.dimen.md_content_padding_top),
                horizPadding,
                context.getDimensionPixelSize(R.dimen.md_content_padding_bottom)
            )
        }
    }

    override fun customView(view: View, wrapInScrollView: Boolean): MaterialDialogBuilder {
        throw IllegalStateException("You cannot use customView() with color picker.")
    }

    fun initialColor(color: Int): ColorMaterialDialogBuilder = this.apply {
        this.initialColor = color
    }

    fun colorSliders(
        lightSliderEnabled: Boolean,
        alphaSliderEnabled: Boolean,
        colorEditEnabled: Boolean
    ): ColorMaterialDialogBuilder = this.apply {
        this.lightSliderEnabled = lightSliderEnabled
        this.alphaSliderEnabled = alphaSliderEnabled
        this.colorEditEnabled = colorEditEnabled
    }

    fun colorWheel(
        wheelType: Int = WheelType.FLOWER,
        wheelDensity: Int = 10
    ): ColorMaterialDialogBuilder = this.apply {
        this.wheelType = wheelType
        this.wheelDensity = wheelDensity
    }

    @JvmOverloads
    fun colorPicker(
        initialColor: Int,
        lightSliderEnabled: Boolean = true,
        alphaSliderEnabled: Boolean = true,
        colorEditEnabled: Boolean = true,
        wheelType: Int = WheelType.FLOWER,
        wheelDensity: Int = 10
    ): ColorMaterialDialogBuilder = this.apply {
        this.initialColor = initialColor
        this.lightSliderEnabled = lightSliderEnabled
        this.alphaSliderEnabled = alphaSliderEnabled
        this.colorEditEnabled = colorEditEnabled
        this.wheelType = wheelType
        this.wheelDensity = wheelDensity
    }

    fun colorCallback(callback: ColorMaterialDialog.ColorCallback): ColorMaterialDialogBuilder = this.apply {
        this.colorCallback = callback
    }

    fun alwaysCallColorCallback(): ColorMaterialDialogBuilder = this.apply {
        this.alwaysCallColorCallback = true
    }

    @UiThread
    override fun build(): ColorMaterialDialog {
        invalidateColors()

        return ColorMaterialDialog(this).also {
            applyKeyboardMode(it)
        }
    }
}