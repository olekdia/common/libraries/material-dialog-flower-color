plugins {
    id("com.android.application")
    kotlin("android")
}

android {
    compileSdk = Versions.sdk.compile
    buildToolsVersion = Versions.buildTools
    namespace = "com.olekdia.sample"

    defaultConfig {
        applicationId = "com.olekdia.sample"
        minSdk = Versions.sdk.min
        targetSdk = Versions.sdk.target
        versionCode = 1
        versionName = "1.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            isShrinkResources = false
        }
        getByName("debug") {
            isMinifyEnabled = false
            isShrinkResources = false
        }
    }
    compileOptions {
        encoding = "UTF-8"
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlin {
        sourceSets["main"].apply {
            kotlin.srcDir("src/main/kotlin")
        }
    }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

dependencies {
    implementation(Libs.androidx.appcompat)
    implementation(Libs.androidx.material)

    implementation(Libs.olekdia.common_jvm)
    implementation(Libs.olekdia.common_android)
    implementation(Libs.olekdia.materialdialog_core)
    implementation(Libs.olekdia.flower_color_picker)

    implementation(project(":flower-color"))
}