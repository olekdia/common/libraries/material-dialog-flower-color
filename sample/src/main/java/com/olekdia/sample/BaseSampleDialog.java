package com.olekdia.sample;

import android.os.Bundle;

import com.olekdia.materialdialog.MaterialDialog;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

public abstract class BaseSampleDialog extends DialogFragment
    implements MaterialDialog.IButtonCallback {

    public static final String BOTTOM_SHEET_STYLE = "BOTTOM_SHEET_STYLE";

    public abstract String getDialogTag();

    public final void show(final FragmentManager fm) {
        show(fm, new Bundle());
    }

    public final void show(final FragmentManager fm, final boolean useBottomSheetStyle) {
        final Bundle args = new Bundle();
        args.putBoolean(BOTTOM_SHEET_STYLE, useBottomSheetStyle);
        show(fm, args);
    }


    public final void show(final FragmentManager fm, final Bundle args) {
        setArguments(args);
        show(fm, getDialogTag());
    }

    @Override
    public void onAny(MaterialDialog dialog) {
    }

    @Override
    public void onPositive(MaterialDialog dialog) {
    }

    @Override
    public void onNegative(MaterialDialog dialog) {
    }

    @Override
    public void onNeutral(MaterialDialog dialog) {
    }
}