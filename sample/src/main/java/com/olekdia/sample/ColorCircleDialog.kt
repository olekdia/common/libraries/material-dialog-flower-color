package com.olekdia.sample

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.WindowManager
import com.olekdia.common.extensions.toArgbString
import com.olekdia.flowercolorpicker.ColorPickerView.WheelType
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.mdflowercolor.ColorMaterialDialog
import com.olekdia.mdflowercolor.ColorMaterialDialog.ColorCallback
import com.olekdia.mdflowercolor.ColorMaterialDialogBuilder

class ColorCircleDialog : BaseSampleDialog(),
    ColorCallback {

    private var dialog: ColorMaterialDialog? = null

    override fun getDialogTag(): String = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = arguments?.getBoolean(BOTTOM_SHEET_STYLE, false) ?: false

        dialog = ColorMaterialDialogBuilder(requireContext())
            .colorPicker(
                Color.RED,
                lightSliderEnabled = true,
                alphaSliderEnabled = true,
                colorEditEnabled = true,
                wheelType = WheelType.CIRCLE,
                wheelDensity = 6
            )
            .colorCallback(this)
            .alwaysCallColorCallback()
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Choose color")
            .iconRes(R.mipmap.ic_launcher)
            .positiveText("Ok")
            .negativeText("Cancel")
            .callback(this)
            .build()
            .also {
                it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            } as ColorMaterialDialog

        return dialog!!
    }


    override fun onSelection(dialog: MaterialDialog?, selectedColor: Int) {
        ToastHelper.showToast(
            context,
            "Selected color: ${selectedColor.toArgbString()}"
        )
    }

    companion object {
        const val TAG = "COLOR_CIRCLE_DLG"
    }
}