package com.olekdia.sample;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.olekdia.common.extensions.ColorExt;
import com.olekdia.materialdialog.MaterialDialog;
import com.olekdia.mdflowercolor.ColorMaterialDialog;
import com.olekdia.mdflowercolor.ColorMaterialDialogBuilder;

import org.jetbrains.annotations.NotNull;

public class ColorFlowerDialog extends BaseSampleDialog implements ColorMaterialDialog.ColorCallback {

    public static final String TAG = "COLOR_FLOWER_DLG";

    @Override
    public String getDialogTag() {
        return TAG;
    }

    @Override
    @NonNull
    @SuppressWarnings("all")
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final boolean useBottomSheetStyle = getArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, false);

        final MaterialDialog dialog = new ColorMaterialDialogBuilder(getActivity())
                .initialColor(Color.BLUE)
                .colorCallback(this)
                .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
                .title("Choose color")
                .iconRes(R.mipmap.ic_launcher)
                .positiveText("Ok")
                .negativeText("Cancel")
                .callback(this)
                .build();

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return dialog;
    }

    @Override
    public void onSelection(@NotNull MaterialDialog dialog, int selectedColor) {
        ToastHelper.showToast(
            getContext(),
            "Selected color: " + ColorExt.toArgbString(selectedColor)
        );
    }
}