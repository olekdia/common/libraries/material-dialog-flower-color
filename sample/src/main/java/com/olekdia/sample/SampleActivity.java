package com.olekdia.sample;

import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

public class SampleActivity extends AppCompatActivity {

    private RadioButton mBottomSheetStyle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sample);

        final FragmentManager fm = getSupportFragmentManager();

        final RadioGroup styleGroup = findViewById(R.id.dialog_style);
        styleGroup.check(R.id.bottom_sheet_style);
        mBottomSheetStyle = findViewById(R.id.bottom_sheet_style);

        findViewById(R.id.color_flower)
            .setOnClickListener(v -> new ColorFlowerDialog()
                .show(fm, isBottomSheetSelected()));

        findViewById(R.id.color_circle)
            .setOnClickListener(v -> new ColorCircleDialog()
                .show(fm, isBottomSheetSelected()));
    }

    private boolean isBottomSheetSelected() {
        return mBottomSheetStyle.isChecked();
    }
}